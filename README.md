# UITS Maintained Aurora Themes
This is a meta-package for pulling in all UTIS-maintained Aurora themes. The primary use-case for this repo is local development of Aurora plugins and themes.
